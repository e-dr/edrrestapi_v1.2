from flask import send_file


def download_now(path):
    return send_file(path, as_attachment=True)
