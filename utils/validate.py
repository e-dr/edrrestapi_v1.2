import json

import jsonschema
from flask import jsonify
from jsonschema import validate

from utils.base_logger import logger as log


###
# Validates that given input is in json format
###
def validate_isjson(jsonData):
    try:
        json.loads(jsonData)
    except ValueError as err:
        log.warning("SUBMITTED DATA IS NOT IN A VALID JSON FORMAT")
        err = jsonify("The format of data is not a valid json format")
        return err, 400, False
    message = "This is a json"
    log.info("json validated")
    return True


###
# Gets the Schema file for validation
###
def get_schema(schemafile):
    with open(schemafile, "r", encoding="utf-8") as file:
        schema = json.load(file)
        return schema


###
# Validates that all value fields are accounted for
###
def validate_json_schema(jsonData, schemafile):
    log.info("Getting json schema")
    schema_base = get_schema(schemafile)

    try:
        validate(instance=jsonData, schema=schema_base)
        log.info("Validation of json to schema")
    except jsonschema.exceptions.ValidationError as err:
        log.warning("JSON DID NOT VALIDATE TO SCHEMA")
        # print(err)
        return (
            {
                "ERROR": "Given JSON data is Invalid one or more fields are missing or incorrect"
            },
            400,
            False,
        )
    log.info("json is valid to schema")
    message = "Given JSON data is Valid"
    return True


###
# Validates that input values are correct
###
def validate_input(jsondata, cur):
    jdata = jsondata
    custId = []
    for order in range(len(jdata["edrBatch"]["orders"])):
        custId.append(jdata["edrBatch"]["orders"][order]["order"]["edrCustID"])
    upc = []
    manuf = []
    for order in range(len(jdata["edrBatch"]["orders"])):
        for item in range(len(jdata["edrBatch"]["orders"][order]["order"]["packages"])):
            for line in range(
                len(
                    jdata["edrBatch"]["orders"][order]["order"]["packages"][item][
                        "package"
                    ]["orderLine"]
                )
            ):
                upc.append(
                    jdata["edrBatch"]["orders"][order]["order"]["packages"][item][
                        "package"
                    ]["orderLine"][line]["upc"]
                )
                manufacturer = jdata["edrBatch"]["orders"][order]["order"]["packages"][item][
                        "package"
                    ]["orderLine"][line]["manufacturer"]
                if manufacturer == 'Bausch and Lomb':
                    manufacturer = 'BL'
                if manufacturer == 'Alcon (CibaVision)':
                    manufacturer = 'CI'
                if manufacturer == 'Coopervision':
                    manufacturer = 'CO'
                if manufacturer == 'Clerio Vision':
                    manufacturer = 'EX'
                if manufacturer == 'Menicon':
                    manufacturer = 'MN'
                if manufacturer == 'Johnson and Johnson':
                    manufacturer = 'VK'
                if manufacturer == 'Visioneering Technologies, Inc.':
                    manufacturer = 'VT' 
                manuf.append(
                    manufacturer
                )
 
    # print(manuf)
    # log.warning(manuf)  
    log.info("Checking customer number")
    custOK = validate_custId(custId, cur)
    if custOK == True:
        log.info("Validating UPCs")
        upcOK = validate_upc(upc, cur)
        if upcOK == True:
            auth = product_authorized(custId, manuf, cur)
            if auth == True:
                return auth
            else:
                return auth
        else:
            return upcOK
    else:
        return custOK


###
# Validates that provided customer number or numbers are valid in system
###
def validate_custId(custId, cur):
    cid = custId

    for i in cid:
        cur.execute("SELECT CICODE FROM HMI.ICUSTMST WHERE CICODE=?", (i,))
        result = cur.fetchone()
        if not result:
            log.warning("CUST NUMBER DOES NOT MATCH ANT EXISTING CUSTOMER")

            return (
                jsonify("Customer number does not match any valid customers"),
                422,
                False,
            )
        else:
            log.info("Customer number is valid")

            return True


###
# Validates that given upc is a current upc on file
###
def validate_upc(upc, cur):

    for i in upc:
        log.info(i)
        cur.execute(r"SELECT CLCODE FROM HMI.ECLUPCP0 WHERE CLCODE=?", (i,))
        result = cur.fetchone()
        if not result:
            log.warning("INVALID UPC SUBMITTED")
            return (
                jsonify(
                    "One or more UPCs are invalid. Please ensure all UPCs are typed correctly and that your product database is up to date"
                ),
                422,
                False,
            )

        cur.execute(
            r"SELECT CLSTCK FROM HMI.ECLUPCP0 WHERE CLCODE=?",
            (i),
        )
        res = cur.fetchone()
        log.warning(res)
        if res != None:
            for x in res:
                deleted = x
                if deleted == "X":
                    log.warning("DISCONTINUED PRODUCT SUBMITTED")
                    return (
                        jsonify(
                            "One or more chosen products are discontinued please check that your product database is up to date"
                        ),
                        422,
                        False,
                    )

        cur.execute(
            r"SELECT SUBSTRING(PDSTYLE,4,1) FROM HMI.BGPRODP0 JOIN HMI.ECLUPCP0 on PDRULE='EDR' and pdnum=clnum WHERE CLCODE=?",
            (i),
        )
        res2 = cur.fetchone()
        log.warning(res2)
        if res2 != None:
            for d in res2:
                dis = d
                if dis == "D":
                    log.warning("DISCONTINUED PRODUCT SUBMITTED")
                    return (
                        jsonify(
                            "One or more chosen products are discontinued please check that your product database is up to date"
                        ),
                        422,
                        False,
                    )

    log.info("All UPCs are valid")
    return True


def product_authorized(custID, manuf, cur):
    count = 0
    for cust in custID:
        for mf in manuf:
            cur.execute(r"SELECT COUNT(*) FROM hmi.ecblkmfrp0 WHERE blcust=? AND blmfcd=?",(cust,mf))
            result = cur.fetchone()
            count = count + result[0]
            print(count)
    if count == 0:
        print("Authorized Product")
        return True
    else:
        print("Not Authorized Product")
        return (
                jsonify(
                    "The eye care professional is not authorized for one or more of the chosen products."
                ),
                422,
                False,
            )