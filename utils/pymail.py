import smtplib
from email.message import EmailMessage


def send_email(send, receive, subject, content):
    sender = send
    receivers = receive
    sub = subject
    con = content

    msg = EmailMessage()
    msg.set_content(con)
    msg["Subject"] = sub
    msg["From"] = sender
    msg["To"] = receivers

    try:
        smtp_obj = smtplib.SMTP("localhost")
        smtp_obj.set_debuglevel(True)
        smtp_obj.send_message(msg)
        smtp_obj.quit()
        print("Success")
    except smtplib.SMTPException:
        print("Error: unable to send email")
