import json
from datetime import datetime

from flask import jsonify, request
from itoolkit import *
from itoolkit.lib.ilibcall import *
from itoolkit.transport import DatabaseTransport

from utils import connect
from utils import submitOrder as sub
from utils import validate as val
from utils.base_logger import logger as log


##############################
### Main function for placing an order
##############################
def order_in(cur):
    request_data = request.get_json()  # request_data is Python Dict object
    data = json.dumps(request_data)  # data is string of json data
    duplicate = check_duplicate(request_data, cur)
    send_dup = ""
    if duplicate == True:
        dup_ord = get_duplicate_ordernum(request_data, cur)
        # for i in dup_ord:
        #     send_dup = send_dup+" "+str(i)+" "
        # log.warning(send_dup) 
        log.warning(dup_ord)   
        log.warning("DUPLICATE ORDER, Request Rejected")
        return {"Error":"Duplicate order submission, no order created",
                "Order_Nums":dup_ord}
        #return jsonify("Duplicate order submission, no order created. Existing Order Numbers: "+send_dup), 422
    valid = val_input(data, request_data, cur)

    if valid == True:
        submit = submit_order(request_data, cur)
        return submit
    else:
        log.warning("INVALID INPUT DATA: ORDER NOT PROCESSABLE")
        return valid


##############################
### Creates the full batch ID and increments the VNTRN number
##############################
def get_batch_ID(cur):
    now = datetime.now()
    date_string = now.strftime("%Y%m%d")

    vendcode = get_vendcode(cur)
    if vendcode == None:
        vend_prefix = "DEFL"  # A Default Prefix to grab orders if a vendor code has not been found
    else:
        vend_prefix = vendcode

    cur.execute(r"SELECT VNTRN# FROM HMI.VENDCTRL WHERE VENDCN='AP00'")
    result = cur.fetchone()
    for num in result:
        newnum = num + 1
        newnum = int(newnum)
        cur.execute(r"UPDATE HMI.VENDCTRL SET VNTRN#=? WHERE VENDCN='AP00'", (newnum,))
        cur.commit()
        newnum = str(newnum).zfill(8)
    batchID = "O" + vend_prefix + newnum + date_string

    return batchID


##############################
### Gets the isolated batch number by pulling the VNTRN
### number after it has been incremented be get_batch_ID()
##############################
def get_batch_num(cur):
    cur.execute(r"SELECT VNTRN# FROM HMI.VENDCTRL WHERE VENDCN='AP00'")
    result = cur.fetchone()
    for num in result:
        batchNum = num
        batchNum = str(batchNum).zfill(8)

    return batchNum


##############################
### Creates a json file of the original order as a back up
##############################
def create_order_file(request_data, batchID):
    save_path = "order_log/"

    filename = batchID
    try:
        with open(save_path + filename + ".json", "w") as newJson:
            newJson.write(json.dumps(request_data))

        # Append batchId to json flie
        request_data["edrBatch"]["@batchID"] = batchID
        with open(save_path + filename + ".json", "w") as read_file:
            read_file.write(json.dumps(request_data))

        # create_log(request_data, filename) #Old .txt log
        return filename
    except:
        return "Order Not Created", False


##############################
### Creates an order log to be used for tracking purposes if necessary
##############################
def create_log(request_data, filename):
    save_path = "order_log/"
    with open(save_path + filename + ".txt", "w") as newlog:
        newlog.write(json.dumps(request_data))


##############################
### Creates a log of status requests
##############################
def create_status_log(content, filename):
    save_path = "status_log/"
    with open(save_path + filename + ".txt", "a") as statuslog:
        statuslog.write(content + "\n")


##############################
### Checks that the json input is valid
##############################
def val_input(data, request_data, cur):
    jsonOK = val.validate_isjson(data)
    schemafile = "lib/order_schema.json"
    if jsonOK == True:
        schemaOK = val.validate_json_schema(request_data, schemafile)
        if schemaOK == True:
            inputOK = val.validate_input(request_data, cur)
            if inputOK == True:
                return inputOK
            else:
                return inputOK
        else:
            return schemaOK
    else:
        return jsonOK


##############################
### Calls RPG EDR004UAPI with batch number as param
##############################
def call_rpg(batch_number):
    itool = iToolKit()
    itransport = DatabaseTransport(connect.get_connection())
    itool.add(
        iPgm("my_results", "EDR004UAPI").addParm(iData("batch", "10a", batch_number))
    )
    itool.call(itransport)


##############################
### Gets the vendor code to be appended to the Batch ID
##############################
def get_vendcode(cur):
    username = request.authorization["username"]
    password = request.authorization["password"]
    user_auth = username + ":" + password

    cur.execute(r"SELECT CDCODE FROM HMI.UTPCODE WHERE CDDESC=?", (user_auth,))
    result = cur.fetchone()
    for code in result:
        vendcode = code.strip()
    return vendcode


##############################
### Checks for duplicate order submission based on vnedor code and vendor po#
##############################


def check_duplicate(request_data, cur):
    data = request_data
    vendcode = get_vendcode(cur)
    for order in range(len(data["edrBatch"]["orders"])):
        ponum = data["edrBatch"]["orders"][order]["order"]["@poID"]
    cur.execute(
        r"SELECT APIVENDOR, APIPOID FROM HMI.FBTAPI00 WHERE APIVENDOR=? AND APIPOID=?",
        (vendcode, ponum),
    )
    result = cur.fetchall()
    for d in result:
        if d == None:
            return False
        else:
            return True
        
        
##############################
### Gets order number of duplicate orders
##############################


def get_duplicate_ordernum(request_data, cur):
    data = request_data
    dup_nums = {}
    vendcode = get_vendcode(cur)
    for order in range(len(data["edrBatch"]["orders"])):
        ponum = data["edrBatch"]["orders"][order]["order"]["@poID"]
        cur.execute(
            r"SELECT APIIORD, APIPOID FROM HMI.FBTAPI00 WHERE APIVENDOR=? AND APIPOID=?",
            (vendcode, ponum),
        )
        result = cur.fetchall()
        for ord, poid in result:
            dup_nums[str(poid).strip()]=str(ord).strip()
    return dup_nums


##############################
### Order Submission Logic
##############################
def submit_order(request_data, cur):
    batchID = get_batch_ID(
        cur
    )  # Full Batch ID comprised of the vendor code, batch number and sys date at time of order
    batchNum = get_batch_num(
        cur
    )  # Batch number as generated from the VNTRN field in VENDCTRL Table
    orderNum = []
    poId = []
    for order in range(len(request_data["edrBatch"]["orders"])):
        poId.append(request_data["edrBatch"]["orders"][order]["order"]["@poID"])
        custNum = request_data["edrBatch"]["orders"][order]["order"]["edrCustID"]
    created = create_order_file(request_data, batchID)
    if created != False:
        log.info("Submitting Order")
        orderNum = sub.sub_order(batchNum, created, cur)
        if type(orderNum) == list:
            log.info("Order Submitted")
            # Call to RPG Program EDR004UAPI
            call_rpg(batchNum)
            ordpo = {orderNum[i]: poId[i] for i in range(len(orderNum))}
            return {
                "Order Submission": "Accepted",
                "Batch ID ": batchID,
                "Customer Number": custNum,
                "Order Number PO Key Value Pair": ordpo,
            }, 202
        else:
            return orderNum
    else:
        log.warning("ORDER NOT CREATED")
        return created
