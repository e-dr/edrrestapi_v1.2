from flask import jsonify, request

from utils.base_logger import logger as log


##############################
### Checks encoded username and password against authorized users stored in database
##############################
def auth_check(cur):
    username = request.authorization["username"]
    password = request.authorization["password"]
    user_auth = username + ":" + password

    cur.execute("SELECT CDDESC FROM HMI.UTPCODE WHERE CDDESC=?", (user_auth,))
    result = cur.fetchone()
    if not result:
        log.warning("Invalid Username or Password")
        return jsonify("Invalid Username or Password"), 401, False
    else:
        log.info("User is authorized")
        return True
