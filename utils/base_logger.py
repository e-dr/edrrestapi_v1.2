import logging
from datetime import datetime as dt
from logging.config import fileConfig

# Logger base for import into other modules

logger = logging
now = dt.now()
date_string = now.strftime("%Y_%m_%d")
filename = "server_log/" + date_string + "_DEBUG_LOG.log"
logging.filename = filename
fileConfig("config/logging.cfg")
