import json
from decimal import Decimal

from flask import jsonify, request

from utils import data_handler as dh
from utils import validate as val
from utils.base_logger import logger as log


def get_status(cur):
    try:
        request_data = request.get_json()
    except:
        log.info('data not json')
        request_data = request.args


    schemafile = "lib/status_schema.json"
    log.info("Validating status json")
    valid = val.validate_json_schema(request_data, schemafile)
    if valid != True:
        log.warning("Invalid status json")
        return valid

    orderNum = request_data["order_number"]
    if not orderNum:
        return {"INPUT ERROR": "Missing Order Number"}, 400

    custNum = request_data["edrCustID"]
    if not custNum:
        return {"INPUT ERROR": "Missing Customer Number"}, 400

    username = request.authorization["username"]
    password = request.authorization["password"]
    user_auth = username + ":" + password

    cur.execute(r"SELECT cdcode from hmi.utpcode where cddesc=?", (user_auth))
    vendcode = cur.fetchone()

    cur.execute(r"SELECT apicust FROM HMI.fbtapi00 WHERE apicust=?", (custNum))
    custID = cur.fetchone()

    cur.execute(r"SELECT apicustid FROM HMI.fbtapi00 WHERE apicustid=?", (custNum))
    clientcustID = cur.fetchone()

    if clientcustID:
        cur.execute(
            r"SELECT apicust FROM HMI.fbtapi00 WHERE apicustid=?", (clientcustID)
        )
        custID = cur.fetchone()

    if not custID:
        return {"INPUT ERROR": "Sent data did not match any records"}, 404

    for i in custID:
        cust = i
    for v in vendcode:
        code = v

    cur.execute(
        r"SELECT apivendor, apicust FROM HMI.fbtapi00 WHERE apivendor=? AND apicust=?",
        (code, cust),
    )
    match = cur.fetchone()
    if not match:
        return {"ERROR": "Customer is not authorized with vendor"}, 403

    cur.execute(
        r"SELECT APIIORD, APICUST FROM HMI.FBTAPI00 WHERE APIIORD=? AND APICUST=?",
        (orderNum, cust),
    )
    result = cur.fetchone()
    if not result:
        return {
            "INPUT ERROR": "Customer Does Not Have Any Orders Matching Given Order Number"
        }, 404

    ##################################################################################################################################################
    ##########     !!!!!  THIS IS A TEMPORARY FAILSAFE CHECK FOR ERROR HANDLING THAT IS STILL BEING RESOLVED  !!!!!                              #####
    ##################################################################################################################################################
    # cur.execute(r"SELECT APIIORD, PHIORD FROM HMI.FBTAPI00 JOIN HMI.POHDR ON APIIORD=PHIORD WHERE APIIORD=?",(orderNum))                       #####
    # failsafe = cur.fetchone()                                                                                                                  #####
    # if not failsafe:                                                                                                                           #####
    #   return {"ERROR": "There was an issue processing one or more lines of this order and was unable to complete", "ORDER#": orderNum}, 424    #####
    ##################################################################################################################################################
    ##########                                                                                                                                   #####
    ##################################################################################################################################################

    cur.execute(
        r"SELECT apierrcd, apicmt, phtrk, phlnno, phshipd, phtrck, phqshp, phqbko, phqcan, phbackd, phcancd, pdrupc, pdpdes, phsord, phmerc, phtaxs, phshpc FROM hmi.fbtapi00 left join hmi.pohdr on apiiord=phiord and apilnno=phlnno left join hmi.podetl on apiiord=pdiord and apilnno=pdline where apiiord=?",
        (orderNum),
    )
    res = cur.fetchall()
    mylist = []

    orderlist = []

    for (
        errcode,
        errmsg,
        proc,
        lnnum,
        shpdate,
        trcknum,
        qtyshp,
        qtybkord,
        qtycan,
        bkodate,
        candate,
        produpc,
        proddesc,
        invnum,
        merch,
        tax,
        shp,
    ) in res:
        processed = proc
        lineNum = lnnum
        shipDate = shpdate
        qtyShipped = qtyshp
        qtyBackordered = qtybkord
        qtyCancelled = qtycan
        backorderedDT = bkodate
        cancelledDT = candate
        addInfo = ""
        if trcknum:
            tracking = trcknum.strip()
        else:
            tracking = ""
        productUPC = produpc
        if proddesc:
            productDesc = proddesc.strip()
        else:
            productDesc = ""

        invoiceNum = invnum
        if merch:
            merchTotal = merch + tax + shp
        else:
            merchTotal = ""
        invoiceTax = tax
        invoiceFreight = shp
        invoiceDate = shpdate

        errorcode = errcode.strip()
        errormessage = errmsg.strip()
        warningmessage = errmsg.strip() + ": Your order will still be processed"

        if errorcode == "E":
            status = "ERROR"
            addInfo = errormessage

        elif tracking:
            status = "Shipped"
        elif backorderedDT:
            status = "Backordered"
        elif cancelledDT:
            status = "Cancelled"
        elif errorcode == "W":
            status = "WARNING"
            addInfo = warningmessage
        elif processed:
            status = "Processing"
            invoiceNum = ""
            merchTotal = ""
            invoiceTax = ""
            invoiceFreight = ""
            invoiceDate = ""

        else:
            status = "Submitted"
            invoiceNum = ""
            merchTotal = ""
            invoiceTax = ""
            invoiceFreight = ""
            invoiceDate = ""

        if len(tracking) == 12:
            carrier = "FedEx"
            trackinglink = "https://www.fedex.com/fedextrack/?tracknum=" + tracking
        elif len(tracking) == 18:
            carrier = "UPS"
            trackinglink = (
                "https://wwwapps.ups.com/etracking/tracking.cgi?tracknum=" + tracking
            )
        elif len(tracking) == 22:
            carrier = "USPS"
            trackinglink = (
                "https://tools.usps.com/go/TrackConfirmAction.action?tLabels="
                + tracking
            )
        else:
            carrier = "No Carrier Info Available"
            trackinglink = "No Tracking Info Available"
            tracking = "No Tracking Info Available"

        mylist.append(
            {
                "line_number": lineNum,
                "ship_date": shipDate,
                "tracking_number": tracking,
                "carrier": carrier,
                "tracking_url": trackinglink,
                "qty_shipped": qtyShipped,
                "qty_backordered": qtyBackordered,
                "qty_canceled": qtyCancelled,
                "status": status,
                "additional_info": addInfo,
                "product_upc": productUPC,
                "product_desc": productDesc,
                "invoice_number": invoiceNum,
                "merchandise_total": merchTotal,
                "invoice_tax": invoiceTax,
                "invoice_freight": invoiceFreight,
                "invoice_date": invoiceDate,
            }
        )

        cur.execute(
            r"SELECT APIIORD, APIPOID FROM HMI.FBTAPI00 WHERE APIIORD=?", (orderNum)
        )
        odn = cur.fetchall()
    for (ordernumber, ponumber) in odn:
        onumber = ordernumber
        pnumber = ponumber.strip()
    orderlist.append(
        {"ORDER": {"ORDER_NUMBER": onumber, "PO_NUMBER": pnumber, "items": mylist}}
    )

    filename = orderNum
    received_data = json.dumps(request_data, indent=4)
    sent_data = json.dumps(orderlist, default=default, indent=4)

    status_content = (
        "\n Status Requested for order number: "
        + orderNum
        + "\n"
        + received_data
        + "\n Responded with: \n"
        + sent_data
        + "\n"
    )

    dh.create_status_log(status_content, filename)

    return jsonify(orderlist)


def default(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)
