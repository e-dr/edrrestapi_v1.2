import json
import string

from flask import jsonify

from utils import data_handler as dh
from utils import pymail as email
from utils.base_logger import logger as log


###
# Submits the order to the FBTAPI00 Table for further processing
###
def sub_order(batch_num, filename, cur):

    batchNum = batch_num
    orderNum = []
    table_data = get_table_stat("HMI", "FBTAPI00", cur)

    with open("order_log/" + filename + ".json") as jdata:
        data = json.load(jdata)

    vendcode = dh.get_vendcode(cur)
    keyTrans = 0
    batchId = data["edrBatch"]["@batchID"]
    orderFile = filename + ".json"

    for order in range(len(data["edrBatch"]["orders"])):
        poId = data["edrBatch"]["orders"][order]["order"]["@poID"]

        try:
            neworder = get_order_num(cur)
            conflict = check_order_conflict(neworder, cur)
            if conflict:
                email.send_email(
                    "Conflict@e-dr.com",
                    "Kgrover@e-dr.com",
                    "Order Conflict Failsafe",
                    "The order number conflict failsafe code has run the first loop on batch: "
                    + batchNum,
                )
                raise Exception("Conflicting order numbers")
            else:
                orderNum.append(neworder)
        except:
            email.send_email(
                "Conflict@e-dr.com",
                "Kgrover@e-dr.com",
                "Locked Retry",
                "Retrying Order number due to locked file",
            )
            log.error("Retrying retrive order number due to locked file")
            try:
                neworder = get_order_num(cur)
                conflict = check_order_conflict(neworder, cur)
                if conflict:
                    email.send_email(
                        "Conflict@e-dr.com",
                        "Kgrover@e-dr.com",
                        "Order Conflict Failsafe",
                        "The order number conflict failsafe code has run the secong loop on batch: "
                        + batchNum,
                    )
                    raise Exception("Conflicting order numbers")
                else:
                    orderNum.append(neworder)
            except:
                email.send_email(
                    "Conflict@e-dr.com",
                    "Kgrover@e-dr.com",
                    "Locked Fail",
                    "Order Failed due to locked file",
                )
                log.error("Failed to retrive new order number, order failed")
                return jsonify("Internal Server Error, no order created"), 500

        orderIndex = len(orderNum) - 1
        clientCustId = data["edrBatch"]["orders"][order]["order"]["clientCustID"]
        edrCustId = data["edrBatch"]["orders"][order]["order"]["edrCustID"]
        locId = data["edrBatch"]["orders"][order]["order"]["locationID"]
        custName = data["edrBatch"]["orders"][order]["order"]["customerName"]

        if "placedBy" in data["edrBatch"]["orders"][order]["order"]:
            placedBy = data["edrBatch"]["orders"][order]["order"]["placedBy"]
        else:
            placedBy = ""

        for item in range(len(data["edrBatch"]["orders"][order]["order"]["packages"])):
            packId = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["packageID"]
            shipType = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["@shipToType"]
            addressee = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["addressee"]
            patientFirst = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["patientFirst"]
            patientLast = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["patientLast"]
            address1 = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["addressLine1"]
            address2 = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["addressLine2"]
            city = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["city"]
            state = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["stateCode"]
            postCode = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["postalCode"]
            phoneNumIn = (data["edrBatch"])["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["telephone"]
            phoneNumFilter = filter(str.isdigit, phoneNumIn)
            phoneNum = "".join(phoneNumFilter)
            shipMethod = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                "package"
            ]["shipTo"]["requestedShipMethod"]

            for line in range(
                len(
                    data["edrBatch"]["orders"][order]["order"]["packages"][item][
                        "package"
                    ]["orderLine"]
                )
            ):
                lineId = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["@poLineID"]
                upc = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["upc"]


                if "eye" in data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]:
                    eye = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["eye"]
                else:
                    eye = ""
                if eye.upper() in ('RIGHT', 'R', 'OD'):
                    eye = 'OD'
                elif eye.upper() in ('LEFT', 'L', 'OS'):
                    eye = 'OS'
                elif eye.upper() in ('BOTH', 'B', 'OU'):
                    eye = 'OU'
                elif eye.upper() in ('STOCK', 'S', 'ST'):
                    eye = 'ST'
                else:
                    eye = ''
                print(eye)


                qty = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["qty"]
                manufacturer = data["edrBatch"]["orders"][order]["order"]["packages"][
                    item
                ]["package"]["orderLine"][line]["manufacturer"]
                
                if manufacturer == 'Bausch and Lomb':
                    manufacturer = 'BL'
                if manufacturer == 'Alcon (CibaVision)':
                    manufacturer = 'CI'
                if manufacturer == 'Coopervision':
                    manufacturer = 'CO'
                if manufacturer == 'Clerio Vision':
                    manufacturer = 'EX'
                if manufacturer == 'Menicon':
                    manufacturer = 'MN'
                if manufacturer == 'Johnson and Johnson':
                    manufacturer = 'VK'
                if manufacturer == 'Visioneering Technologies, Inc.':
                    manufacturer = 'VT' 
                
                productCode = data["edrBatch"]["orders"][order]["order"]["packages"][
                    item
                ]["package"]["orderLine"][line]["productCode"]
                productDesc = data["edrBatch"]["orders"][order]["order"]["packages"][
                    item
                ]["package"]["orderLine"][line]["productDesc"]
                base = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["base"]
                if len(base) > 18:
                    for elem in string.whitespace:
                        base = base.replace(elem, "")
                    base = base[0:18]
                diameter = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["diameter"]
                if len(diameter) > 18:
                    for elem in string.whitespace:
                        diameter = diameter.replace(elem, "")
                    diameter = diameter[0:18]
                power = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["power"]
                if len(power) > 18:
                    for elem in string.whitespace:
                        power = power.replace(elem, "")
                    power = power[0:18]
                cylinder = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["cylinder"]
                if len(cylinder) > 18:
                    for elem in string.whitespace:
                        cylinder = cylinder.replace(elem, "")
                    cylinder = cylinder[0:18]
                axis = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["axis"]
                if len(axis) > 18:
                    for elem in string.whitespace:
                        axis = axis.replace(elem, "")
                    axis = axis[0:18]
                addPower = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["addPower"]
                if len(addPower) > 18:
                    for elem in string.whitespace:
                        addPower = addPower.replace(elem, "")
                    addPower = addPower[0:18]
                addType = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["addType"]
                if len(addType) > 18:
                    for elem in string.whitespace:
                        addType = addType.replace(elem, "")
                    addType = addType[0:18]
                color = data["edrBatch"]["orders"][order]["order"]["packages"][item][
                    "package"
                ]["orderLine"][line]["color"]
                if len(color) > 25:
                    for elem in string.whitespace:
                        color = color.replace(elem, "")
                    color = color[0:25]
                keyTrans += 1

                fieldchk = [
                    "poId",
                    "clientCustId",
                    "edrCustId",
                    "locId",
                    "custName",
                    "packId",
                    "shipType",
                    "addressee",
                    "patientFirst",
                    "patientLast",
                    "address1",
                    "address2",
                    "city",
                    "state",
                    "postCode",
                    "phoneNum",
                    "shipMethod",
                    "lineId",
                    "upc",
                    "qty",
                    "manufacturer",
                    "productCode",
                    "productDesc",
                ]
                fieldval = [
                    poId,
                    clientCustId,
                    edrCustId,
                    locId,
                    custName,
                    packId,
                    shipType,
                    addressee,
                    patientFirst,
                    patientLast,
                    address1,
                    address2,
                    city,
                    state,
                    postCode,
                    phoneNum,
                    shipMethod,
                    lineId,
                    upc,
                    qty,
                    manufacturer,
                    productCode,
                    productDesc,
                ]

                for i in range(len(fieldchk)):
                    chk_len = field_len_check(fieldval[i], fieldchk[i], table_data)
                    if chk_len:
                        return chk_len

                cur.execute(
                    r"INSERT INTO HMI.FBTAPI00(APIVENDOR, APIBATCH, APITRANS, APIFILE, APIBATCHID, APIPOID, APIIORD, APICUSTID, APICUST, APILOC, APICNAME, APIOLCMT, APIPACKID, APISTO, APISNAME, APIPTFIRST, APIPTLAST, APISADD1, APISADD2, APISCITY, APISSTATE, APISZIP, APISPHONE, APISMETHD, APIPOLINE, APIUPC, APIEYE, APIQTY, APIMANF, APIPROD, APIDESC, APIBSE, APIDIA, APIPWR, APICYL, APIAXIS, APIADD, APIADDTYPE, APICLR) VALUES (?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    (
                        vendcode,
                        batchNum,
                        keyTrans,
                        orderFile,
                        batchId,
                        poId,
                        orderNum[orderIndex],
                        clientCustId,
                        edrCustId,
                        locId,
                        custName,
                        placedBy,
                        packId,
                        shipType,
                        addressee,
                        patientFirst,
                        patientLast,
                        address1,
                        address2,
                        city,
                        state,
                        postCode,
                        phoneNum,
                        shipMethod,
                        lineId,
                        upc,
                        eye,
                        qty,
                        manufacturer,
                        productCode,
                        productDesc,
                        base,
                        diameter,
                        power,
                        cylinder,
                        axis,
                        addPower,
                        addType,
                        color,
                    ),
                )

                cur.commit()

    return orderNum


###
# Gets and increments order number from ICONTROL table
###
def get_order_num(cur):

    while True:
        cur.execute(r"SELECT CORDSQ FROM HMI.ICONTRL")
        result = cur.fetchone()
        for num in result:
            newnum = num + 1
            newnum = int(newnum)
            cur.execute(
                r"UPDATE HMI.ICONTRL SET CORDSQ=? WHERE CORDSQ=?", (newnum, num)
            )
        if cur.rowcount != 1:
            continue
        cur.commit()
        orderNum = newnum
        break

    return orderNum


###
# Check neworder against posummary to prevent order conflict
###


def check_order_conflict(neworder, cur):

    cur.execute(r"SELECT POORD# FROM HMI.POSUMMARY WHERE POORD#=?", (neworder))
    conflict = cur.fetchone()

    if conflict:
        return True
    else:
        return False


###
# Get table schema info
###


def get_table_stat(table_schema, table_name, cur):

    col_dict = {}
    cur.execute(
        r"SELECT SUBSTR(TABLE_NAME,1,10), SUBSTR(COLUMN_NAME,1,10), LENGTH FROM QSYS2.SYSCOLUMNS WHERE TABLE_SCHEMA=? AND TABLE_NAME=?",
        (table_schema, table_name),
    )
    max_char = cur.fetchall()
    for tblname, colname, maxlen in max_char:
        colnametrim = colname.strip()
        table = tblname
        col_dict[colnametrim] = maxlen

    return col_dict


###
# Field value character length check
###


def field_len_check(field_val, field_var, table_data):

    varjson = {
        "poId": "@poID",
        "clientCustId": "clientCustID",
        "edrCustId": "edrCustID",
        "locId": "locationID",
        "custName": "customerName",
        "packId": "packageID",
        "shipType": "@shipToType",
        "addressee": "addressee",
        "patientFirst": "patientFirst",
        "patientLast": "patientLast",
        "address1": "addressLine1",
        "address2": "addressLine2",
        "city": "city",
        "state": "stateCode",
        "postCode": "postalCode",
        "phoneNum": "telephone",
        "shipMethod": "requestedShipMethod",
        "lineId": "@poLineID",
        "upc": "upc",
        "qty": "qty",
        "manufacturer": "manufacturer",
        "productCode": "productCode",
        "productDesc": "productDesc",
    }

    vartable = {
        "poId": "APIPOID",
        "clientCustId": "APICUSTID",
        "edrCustId": "APICUST",
        "locId": "APILOC",
        "custName": "APICNAME",
        "packId": "APIPACKID",
        "shipType": "APISTO",
        "addressee": "APISNAME",
        "patientFirst": "APIPTFIRST",
        "patientLast": "APIPTLAST",
        "address1": "APISADD1",
        "address2": "APISADD2",
        "city": "APISCITY",
        "state": "APISSTATE",
        "postCode": "APISZIP",
        "phoneNum": "APISPHONE",
        "shipMethod": "APISMETHD",
        "lineId": "APIPOLINE",
        "upc": "APIUPC",
        "qty": "APIQTY",
        "manufacturer": "APIMANF",
        "productCode": "APIPROD",
        "productDesc": "APIDESC",
    }

    field_name = varjson[field_var]
    var_length = len(field_val)
    max_len = table_data[vartable[field_var]]

    if var_length > max_len:
        log.warning(
            "Order attempted subbmission of "
            + field_name
            + " data length "
            + str(var_length)
            + ". Max accepted "
            + str(max_len)
            + "."
        )
        return (
            jsonify(
                "Error: Data length exceeded for field "
                + field_name
                + ". Max length "
                + str(max_len)
                + ". Received "
                + str(var_length)
                + "."
            ),
            400,
        )
    else:
        return False
