import cProfile
import io
import pstats
from datetime import date

from flask import Flask, render_template, request, send_file, jsonify
from flask_restful import Api

from utils import auth_check as ac
from utils import connect
from utils import data_handler as dh
from utils import status_handler as st
from utils.base_logger import logger as log

app = Flask(__name__, static_url_path="/api/v1/static")
api = Api(app)

conn = connect.get_connection()
cur = conn.cursor()


@app.route("/api/v1/orders", methods=["GET", "POST"])
def add_order(cur=cur):
    auth = ac.auth_check(cur)
    if auth != True:
        return auth
    else:
        if request.method == "POST":
            log.info("Processing order request")
            ordIn = dh.order_in(cur)
            return ordIn
        else:
            log.info("Getting order status")
            status = st.get_status(cur)
            return status


@app.route("/api/v1/documents", methods=["GET"])
def get_docs():
    return render_template("downloaddocs.html")


@app.route("/api/v1/documents/download)")
def download_docs():
    path = "docs/EDR_API_DOCS.zip"

    return send_file(path, as_attachment=True)


@app.route("/api/v1/testconn", methods=["GET", "POST"])
def check_connection(cur=cur):
    auth = ac.auth_check(cur)
    if auth != True:
        return auth
    else:
        return jsonify("connection successful")

if __name__ == "__main__":
    app.run()
